#!/bin/sh

export output_path="$1"

mkdir -p $output_path

cat > mri.md <<EOF
Merge-Request Inspections
-------------------------

Doc build: \`$(date -R)\`.

EOF

css="https://bootswatch.com/3/bower_components/bootstrap/dist/css/bootstrap.min.css"
pandocize () {
    pandoc  -V pagetitle:"$3" -i "$1" -s --css "$css" -o "$2"
}
convert_body () {
    local dir="$1"
    if [ -d "$dir" ] ; then
        cp $dir/body.md $dir.md
        cat >> $dir.md <<EOF

--------
\`[\`[home](./index.html)\`/\`[MRI](./mri.html)\`]\`
EOF
        pandocize "$dir.md"  "$output_path/$dir.html" "$dir"
        printf -- "* 💪 Stuff at [$dir](./$dir.html).\n" >> mri.md
    else
        printf -- "* 💤 Nothing for \`$dir\`.\n" >> mri.md
        echo "Directory $dir not present"
    fi
}


pandocize README.md $output_path/index.html Merbocop
convert_body partial-smondet-tezos
convert_body partial-tezos-tezos
convert_body full-smondet-tezos
convert_body full-tezos-tezos
cat >> mri.md <<EOF

--------
\`[\`[home](./index.html)\`]\`
EOF
pandocize mri.md $output_path/mri.html MRI

