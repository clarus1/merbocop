open! Base

let dbg f = Fmt.kstrf (Caml.Printf.eprintf "[merbocop-dbg:] %s\n%!") f

module System = struct
  let command_to_string_list cmd =
    let i = Unix.open_process_in cmd in
    let rec loop acc =
      try loop (Caml.input_line i :: acc)
      with _ -> Caml.close_in i ; List.rev acc
    in
    let lines = loop [] in
    let status = Unix.close_process_in i in
    (lines, status)

  let command_to_string_list_or_fail cmd =
    match command_to_string_list cmd with
    | l, Unix.WEXITED 0 ->
        l
    | _, _ ->
        Printf.ksprintf failwith "%S returned non-zero" cmd
end

module Web_api = struct
  let dumb_cache : (string, string * Ezjsonm.value) Hashtbl.t =
    Hashtbl.create (module String)

  let get_json ?data_arg ?(request = `GET) ?private_token url =
    let req_option =
      match request with
      | `GET ->
          "--request GET"
      | `PUT ->
          "--request PUT"
      | `POST ->
          "--request POST"
    in
    let data_option =
      Option.value_map data_arg ~default:"" ~f:(fun s ->
          let f = Caml.Filename.temp_file "curl-" ".data" in
          let o = Caml.open_out f in
          Caml.output_string o s ; Caml.close_out o ; Fmt.str "--data '@%s'" f)
    in
    dbg "curling %S %S %S" data_option req_option (Uri.to_string url) ;
    let cmd =
      Printf.sprintf "sleep 1; curl --silent --show-error %s %s %s %s"
        req_option data_option
        (Option.value_map private_token ~default:"" ~f:(fun t ->
             Fmt.str "--header 'PRIVATE-TOKEN: %s'" t))
        (Caml.Filename.quote (Uri.to_string url))
    in
    match Hashtbl.find dumb_cache cmd with
    | Some (_, sl) ->
        sl
    | None ->
        let lines = System.command_to_string_list_or_fail cmd in
        let json =
          try Ezjsonm.value_from_string (String.concat ~sep:"\n" lines)
          with Ezjsonm.Parse_error (v, e) ->
            Fmt.kstr failwith "Error parsing response: %s, %S" e
              (Ezjsonm.value_to_string v)
        in
        let mnemonic =
          Uri.path url |> String.map ~f:(function '/' -> '_' | c -> c)
        in
        Hashtbl.add dumb_cache ~key:cmd ~data:(mnemonic, json) |> ignore ;
        json

  let dump_cache ~path =
    let ith = ref 0 in
    Fmt.kstr System.command_to_string_list "mkdir -p %s" path |> ignore ;
    dbg "Dumping-cache to %s (%d entries)" path (Hashtbl.length dumb_cache) ;
    Hashtbl.iteri dumb_cache ~f:(fun ~key ~data:(mnemo, json) ->
        Caml.incr ith ;
        let o = Caml.open_out (Fmt.str "%s/%03x-%s" path !ith mnemo) in
        Ezjsonm.value_to_channel ~minify:false o
          Ezjsonm.(
            dict
              [ ("key", `String key)
              ; ("mnemonic", `String mnemo)
              ; ("json", json) ]) ;
        Caml.close_out o)

  let load_cache ~path =
    try
      let files = Caml.Sys.readdir path |> Array.to_list in
      List.iter files ~f:(fun fname ->
          try
            let i = Fmt.kstr Caml.open_in_bin "%s/%s" path fname in
            match Ezjsonm.value_from_channel i with
            | `O
                [ ("key", `String key)
                ; ("mnemonic", `String mnemonic)
                ; ("json", json) ] ->
                Caml.close_in i ;
                Hashtbl.add dumb_cache ~key ~data:(mnemonic, json) |> ignore
            | json ->
                Caml.close_in i ;
                Fmt.kstr failwith "Wrong JSNO: %s"
                  (Ezjsonm.value_to_string json)
          with e -> dbg "Error loading cache: %a" Exn.pp e)
    with e -> dbg "Not loading cache at all: %a" Exn.pp e
end

module Gitlab = struct
  let api_base_uri () = Uri.of_string "https://gitlab.com/api/v4"

  let make_uri ?token ?(parameters = []) path =
    let ( // ) = Caml.Filename.concat in
    let actual_params =
      match token with
      | None ->
          parameters
      | Some t ->
          ("private_token", [t]) :: parameters
    in
    let base = api_base_uri () in
    let path = Uri.path base // path in
    Uri.with_path base path |> fun u -> Uri.add_query_params u actual_params

  let opened_merge_requests ?token ~project () =
    Web_api.get_json ?private_token:token
      (make_uri
         ~parameters:[("state", ["opened"]); ("per_page", ["100"])]
         (Fmt.str "projects/%s/merge_requests" project))

  let merge_request_notes ?token ~project ~mr () =
    Web_api.get_json ?private_token:token
      (make_uri ~parameters:[]
         (Fmt.str "projects/%s/merge_requests/%d/notes" project mr))

  let add_merge_request_note ~token ~project ~mr ~body =
    Web_api.get_json ~request:`POST ~private_token:token
      ~data_arg:(Fmt.str "body=%s" (Uri.pct_encode body))
      (make_uri
         (*
~parameters:[("body", [body])]
 *)
         (Fmt.str "projects/%s/merge_requests/%d/notes" project mr))

  let edit_merge_request_note ~token ~project ~mr ~note ~body =
    dbg "body: %d -> %d" (String.length body)
      (Uri.pct_encode body |> String.length) ;
    Web_api.get_json ~request:`PUT ~private_token:token
      ~data_arg:(Fmt.str "body=%s" (Uri.pct_encode body))
      (make_uri
         (* ~parameters:[("body", [body])] *)
         (Fmt.str "projects/%s/merge_requests/%d/notes/%d" project mr note))

  let project_by_id ~token project_id =
    Web_api.get_json ~private_token:token
      (make_uri (Fmt.str "projects/%d" project_id))

  let merge_request_commits ?token ~project ~mr () =
    Web_api.get_json ?private_token:token
      (make_uri ~parameters:[]
         (Fmt.str "projects/%s/merge_requests/%d/commits" project mr))

  let merge_request_changes ?token ~project ~mr () =
    Web_api.get_json ?private_token:token
      (make_uri ~parameters:[]
         (Fmt.str "projects/%s/merge_requests/%d/changes" project mr))
end

module Jq = struct
  include Ezjsonm

  let get_field f assoc =
    try List.Assoc.find_exn assoc f ~equal:String.equal
    with _ -> Fmt.kstr failwith "Jq.get_field: %S not found" f
end

module Project = struct
  type t =
    { id: int
    ; path_with_namespace: string
    ; http_url_to_repo: string
    ; web_url: string
    ; json: Ezjsonm.value }

  let of_id ~token project_id =
    let json = Gitlab.project_by_id ~token project_id in
    let open Jq in
    let sfield name = get_field name (get_dict json) |> get_string in
    let path_with_namespace = sfield "path_with_namespace" in
    let http_url_to_repo = sfield "http_url_to_repo" in
    let web_url = sfield "web_url" in
    {id= project_id; path_with_namespace; http_url_to_repo; web_url; json}
end

module Merge_request = struct
  module Change = struct
    type t =
      {json: Ezjsonm.value; old_path: string; new_path: string; diff: string}
  end

  module Comment = struct
    type t = {id: int; system: bool; json: Ezjsonm.value}
  end

  module Branch = struct
    type t = {name: string; project: Project.t}
  end

  module Commit = struct
    type t =
      { json: Ezjsonm.value
      ; sha: string
      ; title: string
      ; author_email: string
      ; created_at: string
      ; authored_date: string
      ; committed_date: string }
  end

  type t =
    { id: int
    ; title: string
    ; description: string
    ; project: Project.t
    ; target_branch: Branch.t
    ; source_branch: Branch.t
    ; allow_maintainer_to_push: bool option
    ; wip: bool
    ; last_update: string (* 2019-10-31T16:51:30.195Z *)
    ; json: Ezjsonm.value
    ; commits: Commit.t list
    ; changes: Change.t list
    ; comments: Comment.t list }

  type mr_result =
    (t, [`Mr_not_found of string * string * Ezjsonm.value]) Result.t

  let pp_quick ppf =
    let open Fmt in
    function
    | Ok {id; _} ->
        pf ppf "MR-%d" id
    | Error (`Mr_not_found (_, ci, _)) ->
        pf ppf "Error@%s" ci

  let of_json ~token ~project json_obj =
    let open Jq in
    try
      let id = get_field "iid" json_obj |> get_int in
      let comments =
        Gitlab.merge_request_notes ~token ~project ~mr:id ()
        |> get_list (fun v ->
               let obj = get_dict v in
               { Comment.id= get_field "id" obj |> get_int
               ; system= get_field "system" obj |> get_bool
               ; json= v })
      in
      let gs s = get_field s json_obj |> get_string in
      let gb s = get_field s json_obj |> get_bool in
      let gi s = get_field s json_obj |> get_int in
      let mr_project = gi "project_id" |> Project.of_id ~token in
      let last_update = gs "updated_at" in
      let title = gs "title" in
      let description = gs "description" in
      let source_branch =
        { Branch.name= gs "source_branch"
        ; project= gi "source_project_id" |> Project.of_id ~token }
      in
      let target_branch =
        { Branch.name= gs "target_branch"
        ; project= gi "target_project_id" |> Project.of_id ~token }
      in
      let commits =
        Gitlab.merge_request_commits ~token ~project ~mr:id ()
        |> get_list (fun json ->
               let dict = get_dict json in
               let sha = get_field "id" dict |> get_string in
               let author_email =
                 get_field "author_email" dict |> get_string
               in
               let title = get_field "title" dict |> get_string in
               let created_at = get_field "created_at" dict |> get_string in
               let authored_date =
                 get_field "authored_date" dict |> get_string
               in
               let committed_date =
                 get_field "committed_date" dict |> get_string
               in
               Commit.
                 { json
                 ; sha
                 ; author_email
                 ; title
                 ; created_at
                 ; authored_date
                 ; committed_date })
      in
      let changes =
        Gitlab.merge_request_changes ~token ~project ~mr:id ()
        |> get_dict |> get_field "changes"
        |> get_list (fun json ->
               let dict = get_dict json in
               let old_path = get_field "old_path" dict |> get_string in
               let new_path = get_field "new_path" dict |> get_string in
               let diff = get_field "diff" dict |> get_string in
               Change.{json; old_path; new_path; diff})
      in
      { id
      ; title
      ; description
      ; project= mr_project
      ; comments
      ; commits
      ; changes
      ; last_update
      ; target_branch
      ; source_branch
      ; wip= gb "work_in_progress"
      ; allow_maintainer_to_push=
          (try Some (gb "allow_maintainer_to_push") with _ -> None)
      ; json= `O json_obj }
    with e ->
      Fmt.kstr failwith "Error parsging JSON: %s, %s" (Exn.to_string e)
        (Ezjsonm.value_to_string ~minify:false (`O json_obj))

  let most_recent_push mr_result =
    match mr_result with
    | Error _ ->
        None
    | Ok {commits; _} ->
        List.fold commits ~init:None ~f:(fun prev Commit.{created_at; _} ->
            match prev with
            | None ->
                Some created_at
            | Some d ->
                Some (String.max d created_at))

  let find_by_commit ~token ~project commit_sha_prefix : mr_result =
    let json = Gitlab.opened_merge_requests ~token ~project () in
    let open Jq in
    List.find_map (get_list get_dict json) ~f:(fun json_obj ->
        if
          String.is_prefix
            (get_field "sha" json_obj |> get_string)
            ~prefix:commit_sha_prefix
        then Some (of_json json_obj ~token ~project)
        else None)
    |> Result.of_option
         ~error:(`Mr_not_found (project, commit_sha_prefix, json))

  let all_from_project ~token ~project =
    let json = Gitlab.opened_merge_requests ~token ~project () in
    let open Jq in
    List.map (get_list get_dict json) ~f:(fun json_obj ->
        try (project, Ok (of_json json_obj ~token ~project))
        with _e ->
          (project, Error (`Mr_not_found (project, "???", `O json_obj))))

  let post_or_edit_mr_comment ?(do_edit_existing = true) ~token ~project ~mr
      ~user_id body_of_prev =
    let my_comment =
      match mr with
      | Ok {comments= _; _} when not do_edit_existing ->
          None
      | Ok {comments; _} ->
          List.find_map comments ~f:(function
            | {Comment.system= true; _} ->
                None
            | {Comment.json; _} ->
                let open Jq in
                let obj = get_dict json in
                if
                  get_field "author" obj |> get_dict |> get_field "id"
                  |> get_int = user_id
                then
                  Some
                    ( get_field "id" obj |> get_int
                    , get_field "body" obj |> get_string )
                else None)
      | Error _ ->
          None
    in
    match (my_comment, mr) with
    | None, Ok {id; _} ->
        let add_result =
          let body = body_of_prev None in
          Gitlab.add_merge_request_note ~token ~project ~mr:id ~body
        in
        Ok (`Added add_result)
    | Some (i, prev_body), Ok {id; _} -> (
        let body = body_of_prev (Some prev_body) in
        let edit_result =
          Gitlab.edit_merge_request_note ~token ~project ~mr:id ~body ~note:i
        in
        match edit_result with
        | `Float 500. ->
            let addres =
              Gitlab.add_merge_request_note ~token ~project ~mr:id ~body
            in
            Ok (`Added addres)
        | _ ->
            Ok (`Edited edit_result) )
    | _, Error e ->
        Error e
end

module Comment_body = struct
  module Md = struct
    open Fmt

    let empty = ""

    let h n s = [str "%s %s" (String.make n '#') s; empty]

    let par l = l @ [empty]

    let verbatim l = ("```" :: l) @ ["```"]

    let plural = function 1 -> "" | _ -> "s"

    let plural_list = function [_] -> "" | _ -> "s"

    let plural_be = function 1 -> "is" | _ -> "are"
  end

  let pipeline_links () =
    match
      (Caml.Sys.getenv_opt "CI_PIPELINE_ID", Caml.Sys.getenv_opt "CI_JOB_ID")
    with
    | Some p, Some j ->
        Fmt.str " (CI-Pipeline [#%s](%s%s)/[%s](%s%s))" p
          "https://gitlab.com/smondet/merbocop/pipelines/" p j
          "https://gitlab.com/smondet/merbocop/-/jobs/" j
    | _, _ ->
        ""

  let diff_adds diff ~substring =
    let lines = String.split diff ~on:'\n' in
    let substring = String.uppercase substring in
    let _, p =
      List.fold ~init:(0, 0) lines ~f:(fun (minuses, pluses) line ->
          match line.[0] with
          | '-' ->
              if String.is_substring (String.uppercase line) ~substring then
                (minuses + 1, pluses)
              else (minuses, pluses)
          | '+' ->
              if String.is_substring (String.uppercase line) ~substring then
                if minuses = 0 then (0, pluses + 1) else (minuses - 1, pluses)
              else (minuses, pluses)
          | exception _ ->
              (* This is pure context, we're between diffs *) (0, pluses)
          | _ ->
              (* This is pure context, we're between diffs *) (0, pluses))
    in
    p > 0

  let re_proto_0 = Re.Posix.compile_pat "src/proto_0.*/lib_protocol"

  let re_proto_alpha = Re.Posix.compile_pat "src/proto_alpha.*/lib_protocol"

  let full_protocols =
    ["proto_alpha"; "proto_005_PsBabyM1"; "proto_006_PsCARTHA"]

  let client_proto_dirs =
    [ "bin_accuser"
    ; "bin_baker"
    ; "bin_endorser"
    ; "lib_client"
    ; "lib_client_commands"
    ; "lib_delegate"
    ; "lib_mempool"
    ; "lib_parameters" ]

  let changes_touching_protocol changes =
    let open Merge_request.Change in
    let if_list c l = if c then l else [] in
    let check_path change predicate =
      if_list (predicate change.old_path) [change.old_path]
      @ if_list (predicate change.new_path) [change.new_path]
    in
    let changes_wrong_protocol change =
      List.dedup_and_sort ~compare:String.compare
        (check_path change (fun p -> Re.execp re_proto_0 p))
    in
    let changes_proto_alpha change =
      check_path change (fun p -> Re.execp re_proto_alpha p)
    in
    let changes_to_any_client change =
      List.concat_map full_protocols ~f:(fun proto ->
          List.concat_map client_proto_dirs ~f:(fun dir ->
              check_path change (fun p ->
                  String.is_substring p ~substring:("src/" ^ proto)
                  && String.is_substring p ~substring:dir)))
    in
    let dedup_strings = List.dedup_and_sort ~compare:String.compare in
    let on_non_empty l f = if Poly.(l = []) then [] else [f l] in
    on_non_empty
      (dedup_strings (List.concat_map changes ~f:changes_wrong_protocol))
      (fun l -> `Changes_wrong_protocol l)
    @ on_non_empty
        (dedup_strings (List.concat_map changes ~f:changes_proto_alpha))
        (fun l -> `Changes_proto_alpha l)
    @ on_non_empty
        (dedup_strings (List.concat_map changes ~f:changes_to_any_client))
        (fun l -> `Changes_to_client_stuff l)

  let mr_section ~ith ~mr_list (mr_res : Merge_request.mr_result) =
    let open Merge_request in
    let open Fmt in
    let open Md in
    let pp_branch ppf
        {Branch.name; project= {Project.web_url; path_with_namespace; _}} =
      pf ppf "[`%s:%s`](%s/tree/%s)" path_with_namespace name web_url name
    in
    let commits_sublist mr =
      let pp_commit ppf {Commit.title; sha; _} =
        pf ppf "*%s* ([`%s`](%s/commits/%s))" title (String.prefix sha 8)
          mr.source_branch.project.Project.web_url sha
      in
      let re = Re.Posix.compile_pat "[a-zA-Z0-9\\-\\_/]+:.*" in
      let max_title_length = 70 in
      let wips, non_matches, too_longs =
        List.fold mr.commits ~init:(0, [], [])
          ~f:(fun (wips, non_matches, too_longs) ({Commit.title; _} as c) ->
            let change_if value f cond = if cond then f value else value in
            let add_commit l = c :: l in
            ( change_if wips Caml.succ
                ( String.is_substring title ~substring:"WIP"
                || String.is_substring title ~substring:"wip" )
            , change_if non_matches add_commit (not (Re.execp re title))
            , change_if too_longs add_commit
                (String.length title > max_title_length) ))
      in
      let if_not_zero x l = if x = 0 then [] else l in
      if_not_zero wips
        [ str "%d tagged “WIP”%s." wips
            ( if mr.wip then ""
            else " (⚠ while the MR is **not** tagged `WIP:`)" ) ]
      @ List.map non_matches ~f:(fun ci ->
            str "⚠ Title should be `Component: action`: %a." pp_commit ci)
      @ List.map too_longs ~f:(fun ci ->
            str "⚠ Title too long (≥ %d characters): %a." max_title_length
              pp_commit ci)
    in
    let changes_sublist mr =
      let if_changes_path p l =
        if
          List.exists mr.changes ~f:(function
            | {old_path; _} when p old_path ->
                true
            | _ ->
                false)
        then l
        else []
      in
      if_changes_path
        (String.equal ".gitlab-ci.yml")
        [str "🚧 There is a change to the CI config."]
      @ if_changes_path
          (fun p ->
            String.equal (Caml.Filename.basename p) "dune"
            || String.is_suffix (Caml.Filename.basename p) ~suffix:".opam")
          [ ( if String.is_suffix mr.source_branch.name ~suffix:"-opam" then
              str
                "💪 There are changes to the build-system and the branch is \
                 called `%s`."
                mr.source_branch.name
            else
              str
                "🚧 There are changes to build-system: **Did you run the \
                 opam CI?** (i.e. push a branch matching `*-opam`)." ) ]
      @ ( if
          List.for_all mr.changes ~f:(fun c ->
              List.exists
                ~f:(fun suffix ->
                  String.is_suffix (Caml.Filename.basename c.old_path) ~suffix)
                [".rst"; ".md"])
        then [str "This is a doc-only merge-request 📖"]
        else [] )
      @ List.concat_map mr.changes ~f:(fun chg ->
            let should_not_contain substring =
              if diff_adds chg.diff ~substring then
                [ str "⚠ Changes to `%s` contain the string `%s`."
                    chg.old_path substring ]
              else []
            in
            should_not_contain "TODO" @ should_not_contain "WIP")
      @ List.map
          (changes_touching_protocol mr.changes)
          ~f:
            (let pp_path_list ppf l =
               list ~sep:(const string ", ")
                 (fun ppf path -> pf ppf "`%s`" path)
                 ppf l
             in
             function
             | `Changes_wrong_protocol path_list ->
                 str "⚠ There are changes to an *immutable* protocol: %a"
                   pp_path_list path_list
                 (* (String.concat ~sep:", " (List.map ~f:(str "`%s`") path_list)) *)
             | `Changes_proto_alpha path_list ->
                 str
                   "⚠ There are changes to proto-alpha: %a → Please use \
                    the proto-proposal branch for protocol development."
                   pp_path_list path_list
             | `Changes_to_client_stuff path_list ->
                 str
                   "⚠ There are changes to protocol-client-code: %a → \
                    Please make sure changes are properly replicated for all \
                    relevant protocols (e.g. %a)."
                   pp_path_list path_list pp_path_list full_protocols)
    in
    let if_multi yes no = match mr_list with [_] -> no () | _ -> yes () in
    let if_multi_list yes = if_multi yes (fun () -> []) in
    let counter_string =
      if_multi
        (fun () -> str "[%d/%d] " (ith + 1) (List.length mr_list))
        (fun () -> "")
    in
    let title_style = "color: #700" in
    match mr_res with
    | Ok mr ->
        kstr (h 3) "%sMerge-Request [%s!%d](%s/merge_requests/%d)"
          counter_string mr.project.Project.path_with_namespace mr.id
          mr.project.Project.web_url mr.id
        @ if_multi_list (fun () ->
              let title =
                str "<span style=%S>%s</span>" title_style mr.title
              in
              match String.strip mr.description with
              | "" ->
                  par [title]
              | _ ->
                  par
                    [ str
                        "<details><summary>%s</summary><blockquote>\n\
                         %s\n\
                         </blockquote></details>"
                        title mr.description ])
        @ par
            ( ( if mr.wip then [str "* This MR is %s." "**Work-In-Progress**"]
              else [] )
            @ [ str "* This MR aims at merging %a into %a." pp_branch
                  mr.source_branch pp_branch mr.target_branch
              ; str "* The description is %s."
                  ( match String.strip mr.description with
                  | "" ->
                      "**empty** 👎"
                  | more ->
                      let chars, lines =
                        String.fold more ~init:(0, 1) ~f:(fun (c, l) ->
                          function '\n' -> (c + 1, l + 1) | _ -> (c + 1, l))
                      in
                      str "%d bytes and %d lines long" chars lines )
              ; str "* Last updated on: `%s` (last push: `%s`)." mr.last_update
                  ( Merge_request.most_recent_push (Ok mr)
                  |> Option.value ~default:"Unknown" )
              ; str "* %s"
                  ( match mr.allow_maintainer_to_push with
                  | Some true ->
                      "👍 Maintainers can push to the branch. "
                  | Some false ->
                      str
                        "⚠ You **should** [edit your MR](%s) to allow \
                         maintainers to push to your branch (this allows us \
                         to rebase before merging)."
                        "https://docs.gitlab.com/ee/user/project/merge_requests/allow_collaboration.html#enabling-commit-edits-from-upstream-members"
                  | None ->
                      "This project does not support the \
                       `allow-maintainer-to-push` option." )
              ; (let user_comments =
                   List.fold ~init:0 mr.comments ~f:(fun prev ->
                     function
                     | {system= true; _} ->
                         prev
                     | {system= false; _} ->
                         prev + 1)
                 in
                 str "* There %s %d comment%s so far."
                   (plural_be user_comments) user_comments
                   (plural user_comments))
              ; str "* It's made of %d commit%s." (List.length mr.commits)
                  (plural_list mr.commits) ]
            @ (commits_sublist mr |> List.map ~f:(str "    * %s"))
            @
            let intro =
              str "* From the %d change%s, I have %s" (List.length mr.changes)
                (plural_list mr.changes)
            in
            match changes_sublist mr with
            | [] ->
                [intro "no remarks."]
            | [one] ->
                [kstr intro "one remark:"; str "    * %s" one]
            | more ->
                [kstr intro "a couple of remarks:"]
                @ List.map more ~f:(str "    * %s") )
    | Error (`Mr_not_found (proj, c, json)) ->
        kstr (h 3) "%sCommit `%s/%s`" counter_string proj c
        @ par [str "No MR found out (from open MRs in project %s)." proj]
        @ verbatim
            [String.prefix (Jq.value_to_string ~minify:false json) 300; "..."]

  let make ~mr_list () =
    let open Fmt in
    let open Md in
    let intro =
      par
        [ str
            "I, 🤖 👮, ran this inspection on `%s`%s. I usually run \
             inspections every 6 hours and will try to edit my own comment if \
             I have any updates."
            ( System.command_to_string_list "date -R"
            |> fst |> String.concat ~sep:"" )
            (pipeline_links ()) ]
      @ par
          [ str
              "Thank you for your \
               [contribution](https://tezos.gitlab.io/developer/contributing.html) \
               🙏" ]
    in
    let footer =
      [""; String.make 72 '-'; ""]
      @ par
          [ str
              "For more information about this bot-message see the Tezos \
               [documentation](%s) and the [Merbocop](%s) project."
              "http://tezos.gitlab.io/developer/merge_team.html#the-merge-request-bot"
              "https://gitlab.com/smondet/merbocop/" ]
    in
    let lines =
      h 2 "Merbocop Inspection Report"
      @ intro
      @ List.concat_mapi mr_list ~f:(fun ith mr ->
            try mr_section ~ith ~mr_list mr
            with e ->
              par [str "mr failed to make up a section: `%a`" Exn.pp e])
      @ footer
    in
    String.concat ~sep:"\n" lines
end

let say f = Fmt.kstrf (Caml.Printf.eprintf "[merbocop:] %s\n%!") f

let () =
  let open Cmdliner in
  let open Term in
  let help = Term.(ret (pure (`Help (`Auto, None))), info "merbocop") in
  let access_token_term () =
    Arg.(
      pure (fun s -> `Access_token s)
      $ required
          (opt (some string) None
             (info ["access-token"] ~doc:"Gitlab API access token.")))
  in
  let project_id_term () =
    Arg.(
      pure (fun s -> `Project_id s)
      $ required
          (opt (some string) None
             (info ["project-id"] ~doc:"Gitlab API project ID.")))
  in
  let self_id_term () =
    Arg.(
      pure (fun s -> `Self_id s)
      $ required
          (opt (some int) None
             (info ["self-id"] ~doc:"Gitlab API user ID of the bot.")))
  in
  let commits_to_inspect_term () =
    Arg.(
      pure (fun l -> `Commits_to_inspect l)
      $ value
          (opt_all (t2 ~sep:':' int string) []
             (info ["inspect-commit"] ~doc:"project:commit to inspect")))
  in
  let whole_project_inspection_term () =
    Arg.(
      pure (fun s -> `Inspect_whole_projects s)
      $ value
          (opt_all
             (pair ~sep:':' string (some int))
             []
             (info ["whole-project-inspection"] ~doc:"Gitlab API project ID.")))
  in
  let limit_to_recent_updates_term () =
    term_result
      Arg.(
        pure (fun since for_units ->
            match (for_units, since) with
            | None, None ->
                Ok (`Only_updated_since None)
            | None, (Some _ as s) ->
                Ok (`Only_updated_since s)
            | Some (days, `Days), None ->
                let span =
                  let seconds = Float.(days * 60. * 60. * 24.) in
                  Option.value_exn ~message:"ptime span"
                    (Ptime.Span.of_float_s seconds)
                in
                let now =
                  Option.value_exn ~message:"ptime now"
                    (Ptime.of_float_s (Unix.gettimeofday ()))
                in
                let date =
                  Option.value_exn ~message:"ptime sub"
                    (Ptime.sub_span now span)
                in
                let as_string = Ptime.to_rfc3339 date in
                Ok (`Only_updated_since (Some as_string))
            | Some (_months, `Months), None ->
                Error (`Msg "TODO months")
            | Some _, Some _ ->
                Error
                  (`Msg
                    "Cannot provide --only-updated-since and --only-in-the-past"))
        $ value
            (opt (some string) None
               (info ["only-updated-since"]
                  ~doc:"Restrict to MRs updated since." ~docv:"RAW-STING-DATE"))
        $ value
            (opt
               (some
                  (pair ~sep:':' float
                     (enum [("days", `Days); ("months", `Months)])))
               None
               (info ["only-updated-in-the-past"] ~doc:"Restrict MRs")))
  in
  let mr_to_comment_on_term () =
    Arg.(
      pure (fun l -> `Mr_to_comment l)
      $ value
          (opt
             (some (t2 ~sep:':' int string))
             None
             (info ["post-on-mr"] ~doc:"project:commit to select an MR")))
  in
  let inspect_mr_cmd =
    ( ( pure
          (fun (`Access_token token)
               (`Project_id project)
               (`Self_id self)
               commit_sha
               ->
            let mr = Merge_request.find_by_commit ~token ~project commit_sha in
            let body_of_prev _previous_body =
              try Comment_body.make () ~mr_list:[mr] with _ -> "ERRRROOORRR"
            in
            match
              Merge_request.post_or_edit_mr_comment ~token ~user_id:self ~mr
                ~project body_of_prev
            with
            | Ok (`Added add_result) ->
                say "Added: %s"
                  (Ezjsonm.value_to_string ~minify:false add_result) ;
                Stdlib.exit 0
            | Ok (`Edited edit_result) ->
                say "Edited: %s"
                  (Ezjsonm.value_to_string ~minify:false edit_result) ;
                Stdlib.exit 0
            | Error (`Mr_not_found (proj, s, js)) ->
                say "Cannot comment, no Mr there: %s/%s %s" proj s
                  (Ezjsonm.value_to_string ~minify:false js) ;
                Stdlib.exit 1)
      $ access_token_term () $ project_id_term () $ self_id_term ()
      $ Arg.(required (pos 0 (some string) None (info [] ~doc:"COMMIT SHA")))
      )
    , info "inspect-mr" ~doc:"Run an inspection on an MR." )
  in
  let multi_inspect_cmd =
    ( ( pure
          (fun (`Access_token token)
               (`Self_id self)
               (`Commits_to_inspect to_inspect)
               (`Allow_failures allow_failures)
               (`Inspect_whole_projects whole_projects_to_inspect)
               (`Only_updated_since date_opt)
               (`Mr_to_comment mr_to_comment_on_opt)
               (`Comment_on_self comment_on_self)
               (`Edit_existing do_edit_existing)
               (`Write_body write_body)
               (`Load_cache opt_load_path)
               (`Dump_cache opt_path)
               ->
            Option.iter opt_load_path ~f:(fun path -> Web_api.load_cache ~path) ;
            Caml.at_exit (fun () ->
                Option.iter opt_path ~f:(fun path -> Web_api.dump_cache ~path)) ;
            let errors = ref [] in
            let mr_list =
              List.map to_inspect ~f:(fun (project_id, ci) ->
                  let project = Int.to_string project_id in
                  (project, Merge_request.find_by_commit ~token ~project ci))
              @ List.concat_map whole_projects_to_inspect
                  ~f:(fun (project, limit) ->
                    Merge_request.all_from_project ~token ~project
                    |> (fun l ->
                         match date_opt with
                         | None ->
                             l
                         | Some date ->
                             List.filter l ~f:(fun (_, mrres) ->
                                 match
                                   Merge_request.most_recent_push mrres
                                 with
                                 | None ->
                                     false
                                 | Some d ->
                                     String.compare d date >= 0))
                    |> fun l ->
                    match limit with Some lim -> List.take l lim | None -> l)
            in
            errors :=
              !errors
              @ List.filter_map mr_list ~f:(function
                  | _, Ok _ ->
                      None
                  | _, Error (`Mr_not_found (proj, ci, _)) ->
                      Some (Fmt.str "MR-not-found: %s/%s" proj ci)) ;
            if comment_on_self then
              List.iter mr_list ~f:(fun (project, mr) ->
                  (* say "Sleeping 3 seconds" ;
                   * Unix.sleepf 3. ; *)
                  let body_of_prev _previous_body =
                    try Comment_body.make () ~mr_list:[mr]
                    with e ->
                      Fmt.str "ERROR: %a (%d MRs)" Exn.pp e
                        (List.length mr_list)
                  in
                  match
                    Merge_request.post_or_edit_mr_comment ~token ~user_id:self
                      ~do_edit_existing:true ~mr ~project body_of_prev
                  with
                  | Ok (`Added res) | Ok (`Edited res) -> (
                      say "Ok, done %s/%a %s." project Merge_request.pp_quick
                        mr
                        ( Ezjsonm.value_to_string ~minify:true res
                        |> fun s -> String.prefix s 35 ) ;
                      errors :=
                        !errors
                        @
                        match res with
                        | `O [("message", `String msg)] ->
                            [ Fmt.str "Add-or-edit-comment: %s/%a: %s" project
                                Merge_request.pp_quick mr msg ]
                        | _ ->
                            [] )
                  | Error (`Mr_not_found (proj, s, js)) ->
                      say "Cannot comment, no MR there: %s/%s %s" proj s
                        (Ezjsonm.value_to_string ~minify:false js)) ;
            let body_of_prev _previous_body =
              try Comment_body.make () ~mr_list:(List.map ~f:snd mr_list)
              with e ->
                errors :=
                  !errors @ [Fmt.str "Making multi-mr body: %a" Exn.pp e] ;
                Fmt.str "ERROR: %a (%d MRs)" Exn.pp e (List.length mr_list)
            in
            let tmp_md =
              match write_body with
              | Some p ->
                  p
              | None ->
                  Caml.Filename.temp_file "body" ".md"
            in
            let o = Caml.open_out tmp_md in
            Caml.output_string o (body_of_prev None) ;
            Caml.close_out o ;
            say "Body: file://%s\n" tmp_md ;
            Option.iter mr_to_comment_on_opt
              ~f:(fun (project_to_comment_on, commit_to_mr_to_comment_on) ->
                let project = project_to_comment_on |> Int.to_string in
                let mr_for_comment =
                  Merge_request.find_by_commit ~token ~project
                    commit_to_mr_to_comment_on
                in
                match
                  Merge_request.post_or_edit_mr_comment ~token ~user_id:self
                    ~do_edit_existing ~mr:mr_for_comment ~project body_of_prev
                with
                | Ok (`Added res) | Ok (`Edited res) ->
                    say "Ok, done: %s."
                      (Ezjsonm.value_to_string ~minify:false res)
                | Error (`Mr_not_found (proj, s, js)) ->
                    say "Cannot comment, no MR there: %s/%s %s" proj s
                      (Ezjsonm.value_to_string ~minify:false js)) ;
            match !errors with
            | [] ->
                Stdlib.exit 0
            | more ->
                say "There were errors:" ;
                List.iter more ~f:(say "- %s") ;
                Stdlib.exit (if allow_failures then 0 else 3))
      $ access_token_term () $ self_id_term () $ commits_to_inspect_term ()
      $ Arg.(
          pure (fun s -> `Allow_failures s)
          $ value
              (flag (info ["allow-failures"] ~doc:"Return 0 for some errors.")))
      $ whole_project_inspection_term ()
      $ limit_to_recent_updates_term ()
      $ mr_to_comment_on_term ()
      $ Arg.(
          pure (fun s -> `Comment_on_self s)
          $ value (flag (info ["post-on-self"] ~doc:"Post")))
      $ Arg.(
          pure (function
            | true ->
                `Edit_existing false
            | false ->
                `Edit_existing true)
          $ value
              (flag
                 (info ["no-comment-editing"]
                    ~doc:"Always add the comment, do not edit an existing one.")))
      $ Arg.(
          pure (fun s -> `Write_body s)
          $ value
              (opt (some string) None
                 (info ["write-body"]
                    ~doc:"write the body of the messge to PATH (makrdown)."
                    ~docv:"PATH")))
      $ Arg.(
          pure (fun s -> `Load_cache s)
          $ value
              (opt (some string) None
                 (info ["load-cache"]
                    ~doc:"Load contents of the cache from PATH")))
      $ Arg.(
          pure (fun s -> `Dump_cache s)
          $ value
              (opt (some string) None
                 (info ["dump-cache"] ~doc:"Dump contents of the cache in PATH")))
      )
    , info "full-inspection" ~doc:"Do full inspection on a bunch of MRs" )
  in
  exit
    (eval_choice (help : unit Term.t * _) [inspect_mr_cmd; multi_inspect_cmd])
